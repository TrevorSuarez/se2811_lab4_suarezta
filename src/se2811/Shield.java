/**
 * SE2811
 * Winter 2017
 * Lab 4 The Flowers and the Bees - Shield Class
 * Name: Trevor Suarez
 * Created: 1/16/2018
 */

package se2811;

import java.util.ArrayList;

public class Shield extends BeeDecorator {

    private  boolean hasShield;
    private int energy = 100;
    private ArrayList<Flower> flowers;

    public Shield(Bee bee) {

        hasShield = true;

    }

    public void removeShield() {
        hasShield = false;
    }


    public int getEnergy() {
        return energy;
    }

    public void setFlowers(ArrayList<Flower> flowers) {
        this.flowers = flowers;
    }

    /**
     * modifyEnergy subtracts one energy value when one tick is completed
     */
    public void modifyEnergy() {
        if (!hasShield) {
            energy = energy - 1;
        }
    }
}

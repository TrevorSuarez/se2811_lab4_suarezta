/**
 * SE2811
 * Winter 2017
 * Lab 4 The Flowers and the Bees - Venus Trap Class
 * Name: Trevor Suarez
 * Created: 1/16/2018
 */

package se2811;

import javafx.scene.image.ImageView;

/**
 * @author Noe Gonzalez
 * A venus fly trap is a flower that "traps" the bee and provides
 * "negative" nectar, or providing negative energy causing the bee to tire out more,
 * can be visited multiple times, doesn't get reset.
 */
public class VenusTrap extends Flower {
    private int energy;
    private ImageView image;

    /**
     * VenusTrap constructor initializes venustrap instance variables
     * @param image
     */
    public VenusTrap(ImageView image) {
        this.energy = -10;
        this.image = image;
    }

    /**
     * determineEnergy returns energy of venustrap
     * @return energy   energy of venustrap
     */
    @Override
    public int determineEnergy() {
        return energy;
    }

    private boolean hasVisited() {
        return false;
    }

    public double getX() {
        return image.getLayoutX();
    }

    public double getY() {
        return image.getLayoutY();
    }

    public void setEnergy(int energy){
        this.energy = energy;
    }
}
/**
 * SE2811
 * Winter 2017
 * Lab 4 The Flowers and the Bees - Bee Class
 * Name: Trevor Suarez
 * Created: 1/16/2018
 */

package se2811;


/**
 * @author Trevor Suarez
 * Bee class is the parent class of the bumble bee and honey bee, declaring general variables
 */
public abstract class Bee extends GardenObject {

    private int energy;
    private double x;
    private double y;

    public Bee() {

    }


}
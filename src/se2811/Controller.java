/**
 * SE2811
 * Winter 2017
 * Lab 4 The Flowers and the Bees - Controller Class
 * Name: Trevor Suarez
 * Created: 1/16/2018
 */

package se2811;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * @author Trevor Suarez & Noe Gonzalez
 * Controller class connects java functionality with the javafx user interface
 * Controller class also handles
 */
public class Controller implements Initializable {
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private ImageView sunImage1;
    @FXML
    private ImageView sunImage2;
    @FXML
    private ImageView sunImage3;
    @FXML
    private ImageView venusImage1;
    @FXML
    private ImageView venusImage2;
    @FXML
    private ImageView venusImage3;
    @FXML
    private ImageView bumbleImage;
    @FXML
    private ImageView honeyImage;
    @FXML
    private ImageView speedImage;
    @FXML
    private ImageView shieldImage;
    @FXML
    private Label honeyLife;
    @FXML
    private Label bumbleLife;
    @FXML
    private Label speedLife;
    @FXML
    private Label shieldLife;
    @FXML
    private Label welcomeLabel;

    private BumbleBee bumbleBee;
    private HoneyBee honeyBee;
    private DoubleSpeed speedBee;
    private Shield shieldBee;

    private Sunflower sunflower1;
    private Sunflower sunflower2;
    private Sunflower sunflower3;
    private VenusTrap venus1;
    private VenusTrap venus2;
    private VenusTrap venus3;
    private boolean firstTick;
    private Flower flower;
    private double xDest;
    private double yDest;
    private ArrayList<Flower> flowers;
    private ArrayList<Flower> venusList;

    /**
     * initialize sets all defaults to javafx components
     *
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        firstTick = false;
        xDest = 0;
        yDest = 0;
        //flower = new Sunflower(sunImage2);
        Image sunFlower = new Image("sunflower.png");
        Image venus = new Image("flytrap.png");
        Image bumble = new Image("bumblebee.png");
        Image honey = new Image("honeybee.png");
        Image speed = new Image ("speedbee.png");
        Image shield = new Image("shield.png");
        flowers = new ArrayList<>();
        venusList = new ArrayList<>();
        sunImage1.setImage(sunFlower);
        sunImage2.setImage(sunFlower);
        sunImage3.setImage(sunFlower);
        venusImage1.setImage(venus);
        venusImage2.setImage(venus);
        venusImage3.setImage(venus);
        bumbleImage.setImage(bumble);
        honeyImage.setImage(honey);
        shieldImage.setImage(shield);
        bumbleBee = new BumbleBee(bumbleImage);
        honeyBee = new HoneyBee(honeyImage);
        sunflower1 = new Sunflower(sunImage1);
        sunflower2 = new Sunflower(sunImage2);
        sunflower3 = new Sunflower(sunImage3);
        venus1 = new VenusTrap(venusImage1);
        venus2 = new VenusTrap(venusImage2);
        venus3 = new VenusTrap(venusImage3);
        flowers.add(sunflower1);
        flowers.add(sunflower2);
        flowers.add(sunflower3);
        flowers.add(venus1);
        flowers.add(venus2);
        flowers.add(venus3);
        venusList.add(venus1);
        venusList.add(venus2);
        venusList.add(venus3);

        RedirectVenus venusBee = new RedirectVenus();

        speedImage.setImage(speed);
        Bee beespeed = new BumbleBee(speedImage);
        speedBee = new DoubleSpeed(beespeed);

        Bee sBee = new HoneyBee(shieldImage);
        shieldBee = new Shield(sBee);

        speedBee.setFlowers(flowers);
        bumbleBee.setFlowers(flowers);
        honeyBee.setFlowers(flowers);
        shieldBee.setFlowers(flowers);
        venusBee.setFlowers(venusList);

        sunflower1.setX(120);
        sunflower1.setY(91);
        sunflower1.setEnergy(10);
        sunflower2.setX(140);
        sunflower2.setY(281);
        sunflower2.setEnergy(10);
        sunflower3.setX(498);
        sunflower3.setY(547);
        sunflower3.setEnergy(10);
        venus1.setX(441);
        venus1.setY(118);
        venus2.setX(304);
        venus2.setY(361);
        venus3.setX(282);
        venus3.setY(216);
        bumbleBee.setX(542);
        bumbleBee.setY(275);
        honeyBee.setX(0);
        honeyBee.setY(255);
        speedBee.setX(509);
        speedBee.setY(381);

        anchorPane.setFocusTraversable(true);

    }

    /**
     * onKeyPress handles the right arrow event each time key is pressed
     *
     * @param keyEvent
     */
    @FXML
    public void onKeyPressed(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.RIGHT) {
            tick();
        }
    }

    /**
     * tick handles movement and tracking of overlapping of bees and flowers
     * tick is activated each time right arrow is pressed
     */
    public void tick() {
        welcomeLabel.setVisible(false);
        welcomeLabel.setDisable(true);
        //Subtracts 1 for energy tic
        bumbleBee.modifyEnergy();
        honeyBee.modifyEnergy();
        speedBee.modifyEnergy();
        shieldBee.modifyEnergy();
        bumbleLife.setText("Bumble Bee Life: " + bumbleBee.getEnergy());
        honeyLife.setText("Honey Bee Life: " + honeyBee.getEnergy());
        speedLife.setText("Speed Bee Life: " + speedBee.getEnergy());
        shieldLife.setText("Shield Bee LIfe: " + shieldBee.getEnergy());

        if (bumbleBee.getEnergy() <= 0) {
            bumbleImage.setVisible(false);
            bumbleBee.setEnergy(0);
        }

        for (int i = 0; i < flowers.size(); i++) {
            if (flowers.get(i).getX() == shieldBee.getX()) {
                if (flowers.get(i).getY() == shieldBee.getY()) {
                    shieldBee.removeShield();
                }
            }
        }

        randomDecision(bumbleBee, bumbleImage, bumbleBee.calculateRandCoordinate());
        randomDecision(speedBee, speedImage, speedBee.calculateRandCoordinate());


        if (honeyImage.getY() < 300) {
            honeyBee.setY(honeyBee.getY() + 10);
            honeyImage.setY(honeyImage.getY() + 10);
        } else {
            honeyImage.setX(honeyImage.getX() + 100);
            honeyBee.setX(honeyBee.getX() + 100);
            honeyImage.setY(-250);
            honeyBee.setY(0);
        }

        if (shieldImage.getY() < 250) {
            shieldBee.setY(shieldBee.getY() + 10);
            shieldImage.setY(shieldImage.getY() + 10);
        } else {
            shieldImage.setX(shieldImage.getX() + -100);
            shieldBee.setX(honeyBee.getX() + 100);
            shieldImage.setY(-250);
            shieldBee.setY(0);
        }



        //Checks if bumble and honey collided
        if (bumbleBee.calculateEuclidean(honeyImage.getLayoutX(), honeyImage.getLayoutY()) < 45.0) {
            honeyBee.setEnergy(0);
            bumbleBee.setEnergy(0);
            honeyImage.setVisible(false);
            bumbleImage.setVisible(false);
        }

        for (int i = 0; i < flowers.size(); i++) {
            if (honeyBee.calculateEuclidean(flowers.get(i).getX(), flowers.get(i).getY()) < 45.0) {
                honeyBee.addEnergy(flowers.get(i).determineEnergy());
                flowers.get(i).visit();
            }
        }

        for (int i = 0; i < flowers.size(); i++) {
            if (bumbleBee.calculateEuclidean(flowers.get(i).getX(), flowers.get(i).getY()) < 45.0) {
                bumbleBee.addEnergy(flowers.get(i).determineEnergy());
                flowers.get(i).visit();
            }
        }

        Alert alert = new Alert(Alert.AlertType.WARNING);

        if (bumbleBee.getEnergy() <= 0 && honeyBee.getEnergy() <= 0 && speedBee.getEnergy() <= 0) {
            bumbleImage.setVisible(false);
            bumbleBee.setEnergy(0);
            honeyImage.setVisible(false);
            honeyBee.setEnergy(0);
            speedImage.setVisible(false);
            speedBee.setEnergy(0);

            alert.setTitle("Garden Program");
            alert.setHeaderText("GAME OVER!");
            alert.setContentText("All Bees have died!");
            alert.showAndWait();

            Platform.exit();
        }

    }

    public void randomDecision(Bee methodBee, ImageView methodImage, Flower flower) {
        if (methodBee.getX() < (flower.getX() + 20) && methodBee.getX() > (flower.getX() - 20)) {
            if (methodBee.getY() < (flower.getY() + 20) && methodBee.getY() > (flower.getY() - 20)) {
                xDest = Math.abs(methodImage.getLayoutX() - flower.getX()) / 50;
                yDest = Math.abs(methodImage.getLayoutY() - flower.getY()) / 50;
            }
        } else if (!firstTick) {
            xDest = Math.abs(methodImage.getLayoutX() - flower.getX()) / 50;
            yDest = Math.abs(methodImage.getLayoutY() - flower.getY()) / 50;
            firstTick = true;
        }
        if (methodImage.getLayoutX() <= flower.getX()) {
            methodBee.setX(methodBee.getX() + xDest);
            methodImage.setLayoutX(methodBee.getX());
        } else {
            methodBee.setX(methodBee.getX() - xDest);
            methodImage.setLayoutX(methodBee.getX());
        }
        if (methodImage.getLayoutY() < flower.getY()) {
            methodBee.setY(methodBee.getY() + yDest);
            methodImage.setLayoutY(methodBee.getY());
        } else {
            methodBee.setY(methodBee.getY() - yDest);
            methodImage.setLayoutY(methodBee.getY());
        }


    }

}

/**
 * SE2811
 * Winter 2017
 * Lab 4 The Flowers and the Bees - Main Class
 * Name: Trevor Suarez
 * Created: 1/16/2018
 */

package se2811;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * @author Trevor Suarez & Noe Gonzalez
 * Main initializes the javafx components and launches window
 */
public class Main extends Application {

    /**
     * start prepares the javafx components
     * @param primaryStage  the main stage which houses javafx components
     * @throws Exception    generic exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("bee.fxml"));
        Scene scene = new Scene(root, 600, 600);
        primaryStage.setTitle("Garden");
        primaryStage.setScene(scene);
        String image = "grass.jpg";
        root.setStyle("-fx-background-image: url('" + image + "'); " +
                "-fx-background-position: center center; " +
                "-fx-background-repeat: stretch;");
        primaryStage.show();
    }

    /**
     * main launches javafx window
     * @param args  array of arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}

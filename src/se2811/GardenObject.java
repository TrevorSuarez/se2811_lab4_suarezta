/**
 * SE2811
 * Winter 2017
 * Lab 3 The Flowers and the Bees - GardenObject Class
 * Name: Noe Gonzalez & Trevor Suarez
 * Created: 1/9/2018
 */

package se2811;

import javafx.scene.image.ImageView;

/**
 * @author Trevor Suarez
 * GardenObect is at the domain level. Each flower and bee are garden obkects who use
 * the calculate euclidean and getters and setters to perform their specific operations
 */
public abstract class GardenObject {

    private double euclidean;
    private double x;
    private double y;
    private ImageView image;

    public GardenObject() {

    }


    /**
     * Calculated the euclidean distance between the bee and the flower.
     *
     * @param x x coordinate of the flower
     * @param y y coordinate of the flower
     */
    public double calculateEuclidean(double x, double y) {
        double xPair = Math.abs(this.x - x);
        xPair = xPair * xPair;
        double yPair = Math.abs(this.y - y);
        yPair = yPair * yPair;
        double euclidean = Math.sqrt(xPair + yPair);
        return euclidean;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

}
/**
 * SE2811
 * Winter 2017
 * Lab 4 The Flowers and the Bees - Double Speed Class
 * Name: Trevor Suarez
 * Created: 1/16/2018
 */

package se2811;

import java.util.ArrayList;
import java.util.Random;

public class DoubleSpeed extends BeeDecorator {

    private int energy;
    private double x;
    private double y;
    private ArrayList<Flower> flowers;
    private Random random;

    public DoubleSpeed(Bee bee) {
        random = new Random();
        energy = 100;
        this.x = bee.getX();
        this.y = bee.getY();
    }

    public void setFlowers(ArrayList<Flower> flowers) {
        this.flowers = flowers;
    }

    @Override
    public double getX() {
        return x + 10;
    }

    @Override
    public double getY() {
        return y + 10;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int newEnergy) {
        energy = newEnergy;
    }

    /**
     * modifyEnergy subtracts one energy value when one tick is completed
     */
    public void modifyEnergy() {
        energy = energy - 1;
    }

    /**
     * addEnergy adds energy when hovered over flower
     * @param addEnergy     amount of energy being added to bee
     */
    public void addEnergy(int addEnergy) {
        energy = energy + addEnergy;
    }

    public Flower calculateRandCoordinate() {
        int index = random.nextInt(flowers.size());
        return flowers.get(index);
    }

}

//change


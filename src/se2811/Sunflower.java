/**
 * SE2811
 * Winter 2017
 * Lab 3 The Flowers and the Bees - Sunflower Class
 * Name: Noe Gonzalez & Trevor Suarez
 * Created: 1/9/2018
 */

package se2811;

import javafx.scene.image.ImageView;

/**
 * @author Noe Gonzalez
 * SunFlower is a flower, that provides more energy/nectar than a regular flower
 */
public class Sunflower extends Flower {

    private boolean visited;
    private int energy;
    private ImageView image;
    private double x;
    private double y;

    /**
     * Sunflower constructor initializes sunflower instance variables
     * @param image     imageview which contains sunflower image
     */
    public Sunflower(ImageView image) {
        this.energy = 10;
        this.visited = false;
        this.image = image;
        this.x = image.getLayoutX();
        this.y = image.getLayoutY();

    }


    /**
     * Determines if the flower has nectar by seeing if it has been visited
     * @return energy, 0    returns energy if visited, 0 if no pollen is avaliable
     */
    @Override
    public int determineEnergy() {
        if (visited) {
            return 0;
        } else {
            visited = true;
            return energy;
        }
    }

    public double getX() {
        return image.getLayoutX();
    }

    public double getY() {
        return image.getLayoutY();
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }
}